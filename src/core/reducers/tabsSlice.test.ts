import {
    tabsInitialState,
    tabsReducer,
    setCurrentTabIdAction,
    setTabsIdsAction,
} from './tabsSlice';

describe('Test tabsReducer', () => {
    it('Should succeed setTabsIdsAction change', () => {
        const actionPayload = ['firstTab', 'secondTab'];
        const action = setTabsIdsAction(actionPayload);
        const result = tabsReducer(tabsInitialState, action);

        expect(result.tabsIds).toEqual(actionPayload);
        expect(result.currentTabId).toEqual(tabsInitialState.currentTabId);
    });

    it('Should succeed setCurrentTabIdAction change', () => {
        const actionPayload = 'secondTab';
        const action = setCurrentTabIdAction(actionPayload);
        const result = tabsReducer(tabsInitialState, action);

        expect(result.currentTabId).toBe(actionPayload);
        expect(result.tabsIds).toEqual(tabsInitialState.tabsIds);
    });
});

import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit';

import { IRootState } from '../interfaces/IRootState';

type State = IRootState['tabs'];

export const tabsInitialState: State = {
    tabsIds: [],
    currentTabId: '',
};

export const tabsSlice = createSlice<State, SliceCaseReducers<State>>({
    name: 'tabs',
    initialState: tabsInitialState,
    reducers: {
        setCurrentTabIdAction: (state, action) => {
            state.currentTabId = action.payload;
        },
        setTabsIdsAction: (state, action) => {
            state.tabsIds = action.payload;
        },
    },
});

export const { setCurrentTabIdAction, setTabsIdsAction } = tabsSlice.actions;

export const selectCurrentTabId = (state: IRootState) =>
    state.tabs.currentTabId;
export const selectNextTabId = (state: IRootState) =>
    state.tabs.tabsIds.find(tabId => tabId !== state.tabs.currentTabId);

export const tabsReducer = tabsSlice.reducer;

import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit';

import { IRootState } from '../interfaces/IRootState';
import editorMockData from '../../mocks/editorMockData.json';

type State = IRootState['configEditor'];

export const configEditorInitialState: State = {
    appliedConfig: '{}',
    currentConfig: JSON.stringify(editorMockData, null, '\t'),
};

export const configEditorSlice = createSlice<State, SliceCaseReducers<State>>({
    name: 'editor',
    initialState: configEditorInitialState,
    reducers: {
        setCurrentConfigAction: (state, action) => {
            state.currentConfig = action.payload;
        },
        applyConfigAction: (state, action) => {
            state.appliedConfig = action.payload;
        },
    },
});

export const {
    applyConfigAction,
    setCurrentConfigAction,
} = configEditorSlice.actions;

export const selectCurrentConfig = (state: IRootState) =>
    state.configEditor.currentConfig;
export const selectAppliedConfig = (state: IRootState) =>
    state.configEditor.appliedConfig;

export const configEditorReducer = configEditorSlice.reducer;

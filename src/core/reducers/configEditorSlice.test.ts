import {
    applyConfigAction,
    configEditorInitialState,
    configEditorReducer,
    setCurrentConfigAction,
} from './configEditorSlice';

describe('Test configEditorReducer', () => {
    const testConfig = `{
        "legend": "Foo",
        "items": [
            {
                "type": "text",
                "value": "Bar"
            },
            {
                "type": "number",
                "value": 1
            }
        ]
    }`;

    it('Should succeed setCurrentConfigAction change', () => {
        const actionPayload = testConfig;
        const action = setCurrentConfigAction(actionPayload);
        const result = configEditorReducer(configEditorInitialState, action);

        expect(result.currentConfig).toEqual(actionPayload);
        expect(result.appliedConfig).toEqual(
            configEditorInitialState.appliedConfig,
        );
    });

    it('Should succeed applyConfigAction change', () => {
        const actionPayload = testConfig;
        const action = applyConfigAction(actionPayload);
        const result = configEditorReducer(configEditorInitialState, action);

        expect(result.appliedConfig).toBe(actionPayload);
        expect(result.currentConfig).toBe(
            configEditorInitialState.currentConfig,
        );
    });
});

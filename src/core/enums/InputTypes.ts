export enum InputTypes {
    TEXT = 'text',
    NUMBER = 'number',
    TEXTAREA = 'textarea',
    CHECKBOX = 'checkbox',
    DATE = 'date',
    RADIO = 'radio',
    COLOR = 'color',
    RANGE = 'range',
    PASSWORD = 'password',
}

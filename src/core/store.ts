import { configureStore } from '@reduxjs/toolkit';

import { IRootState } from './interfaces/IRootState';
import { tabsReducer } from './reducers/tabsSlice';
import { configEditorReducer } from './reducers/configEditorSlice';

export default configureStore<IRootState>({
    reducer: {
        tabs: tabsReducer,
        configEditor: configEditorReducer,
    },
});

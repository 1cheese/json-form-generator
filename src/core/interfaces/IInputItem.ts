import { InputTypes } from '../enums/InputTypes';

export interface IInputItem
    extends Omit<HTMLInputElement & HTMLTextAreaElement, 'type'> {
    type: InputTypes;
    label?: string;
}

import * as React from 'react';

import { IInputItem } from './IInputItem';

export interface IQuerySchema {
    legend?: string;
    items?: IInputItem[];
    buttons?: Array<{
        text: string;
        type?: React.ButtonHTMLAttributes<any>['type'];
    }>;
}

export interface IRootState {
    tabs: {
        tabsIds: string[];
        currentTabId: string;
    };
    configEditor: {
        currentConfig: string;
        appliedConfig: string;
    };
}

import styled from '@emotion/styled';

export const Main = styled.div`
    display: grid;
    grid-template-rows: 1fr auto;
    grid-row-gap: 16px;
    height: 100%;
`;

export const ButtonWrapper = styled.div`
    display: flex;
    justify-content: flex-end;
`;

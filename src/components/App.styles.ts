import styled from '@emotion/styled';

export const Main = styled.main`
    padding: 16px;
    width: 100%;
    min-height: 100vh;
    display: flex;
    justify-content: center;
`;

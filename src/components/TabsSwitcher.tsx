import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import * as Styled from './TabsSwitcher.styles';
import {
    selectCurrentTabId,
    setCurrentTabIdAction,
    setTabsIdsAction,
} from '../core/reducers/tabsSlice';

export interface TabsSwitcherProps {
    tabs: Array<{
        title: string;
        component: React.ReactNode;
        id: string;
    }>;
}

export const TabsSwitcher: React.FC<TabsSwitcherProps> = ({ tabs }) => {
    const dispatch = useDispatch();
    const currentTabId = useSelector(selectCurrentTabId);

    React.useEffect(() => {
        const tabsIds = tabs.map(({ id }) => id);

        dispatch(setTabsIdsAction(tabsIds));
        dispatch(setCurrentTabIdAction(tabs[0].id));
    }, [dispatch, tabs]);

    const handleTabClick = (id: string) => {
        dispatch(setCurrentTabIdAction(id));
    };

    if (tabs.length === 0) {
        return null;
    }

    return (
        <Styled.Main>
            <Styled.Tabs>
                {tabs.map(({ title, id }) => (
                    <Styled.Tab
                        isActive={id === currentTabId}
                        key={id}
                        onClick={() => handleTabClick(id)}
                    >
                        {title}
                    </Styled.Tab>
                ))}
            </Styled.Tabs>
            <Styled.Body>
                {tabs.map(
                    ({ component, id }) =>
                        id === currentTabId && (
                            <React.Fragment key={id}>
                                {component}
                            </React.Fragment>
                        ),
                )}
            </Styled.Body>
        </Styled.Main>
    );
};

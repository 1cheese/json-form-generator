import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AceEditor from 'react-ace';
import 'ace-builds/src-noconflict/mode-json';
import 'ace-builds/src-noconflict/theme-github';

import * as Styled from './ConfigEditor.styles';
import {
    applyConfigAction,
    selectCurrentConfig,
    setCurrentConfigAction,
} from '../core/reducers/configEditorSlice';
import {
    selectNextTabId,
    setCurrentTabIdAction,
} from '../core/reducers/tabsSlice';

export const ConfigEditor: React.FC = () => {
    const dispatch = useDispatch();
    const currentConfig = useSelector(selectCurrentConfig);
    const nextTabId = useSelector(selectNextTabId);

    const handleEditorChange = (newValue: string): void => {
        dispatch(setCurrentConfigAction(newValue));
    };

    const handleApplyButtonClick = (): void => {
        dispatch(applyConfigAction(currentConfig));
        dispatch(setCurrentTabIdAction(nextTabId));
    };

    return (
        <Styled.Main>
            <AceEditor
                style={{
                    minHeight: 290,
                    border: '1px solid gray',
                    width: '100%',
                    height: '100%',
                }}
                mode="json"
                theme="github"
                onChange={handleEditorChange}
                fontSize={14}
                value={currentConfig}
                setOptions={{
                    showLineNumbers: true,
                    useWorker: false,
                    autoScrollEditorIntoView: true,
                }}
            />
            <Styled.ButtonWrapper>
                <button onClick={handleApplyButtonClick}>Apply</button>
            </Styled.ButtonWrapper>
        </Styled.Main>
    );
};

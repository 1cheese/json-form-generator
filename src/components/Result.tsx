import * as React from 'react';
import { useSelector } from 'react-redux';

import * as Styled from './Result.styles';
import { selectAppliedConfig } from '../core/reducers/configEditorSlice';
import { IQuerySchema } from '../core/interfaces/IQuerySchema';
import { IInputItem } from '../core/interfaces/IInputItem';
import { InputTypes } from '../core/enums/InputTypes';

const renderInputs = (
    inputItem: IInputItem,
    index: number,
): React.ReactNode => {
    const id = `id_${index}`;
    const elementType =
        inputItem.type === InputTypes.TEXTAREA ? 'textarea' : 'input';

    return (
        <React.Fragment>
            {inputItem.label !== undefined && (
                <Styled.Label>
                    <label htmlFor={id}>{inputItem.label}</label>
                </Styled.Label>
            )}
            <Styled.Input>
                {React.createElement(elementType, {
                    ...inputItem,
                    id: id,
                    defaultValue: inputItem.value,
                    value: undefined,
                    defaultChecked: inputItem.checked,
                    checked: undefined,
                })}
            </Styled.Input>
        </React.Fragment>
    );
};

export const Result: React.FC = () => {
    const appliedConfig = useSelector(selectAppliedConfig);

    try {
        const parsedConfig: IQuerySchema = JSON.parse(appliedConfig);
        const { legend, items, buttons } = parsedConfig;

        if (items === undefined || items.length === 0) {
            return <Styled.Warning>Please enter valid config.</Styled.Warning>;
        }

        return (
            <form onSubmit={event => event.preventDefault()}>
                <fieldset>
                    {legend && <legend>{legend}</legend>}

                    {items.map((input, index) => (
                        <Styled.Row key={index}>
                            {renderInputs(input, index)}
                        </Styled.Row>
                    ))}
                </fieldset>
                {buttons !== undefined && buttons.length > 0 && (
                    <Styled.ButtonsWrapper>
                        {buttons.map((button, index) => (
                            <button key={index} type={button?.type}>
                                {button.text}
                            </button>
                        ))}
                    </Styled.ButtonsWrapper>
                )}
            </form>
        );
    } catch (error) {
        console.error(error);
        return <Styled.Warning>JSON syntax error.</Styled.Warning>;
    }
};

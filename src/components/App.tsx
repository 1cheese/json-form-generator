import * as React from 'react';
import { nanoid } from 'nanoid';

import * as Styled from './App.styles';
import { TabsSwitcher, TabsSwitcherProps } from './TabsSwitcher';
import { ConfigEditor } from './ConfigEditor';
import { Result } from './Result';

function App() {
    const tabs: TabsSwitcherProps['tabs'] = [
        {
            title: 'Config',
            component: <ConfigEditor />,
            id: nanoid(),
        },
        {
            title: 'Result',
            component: <Result />,
            id: nanoid(),
        },
    ];

    return (
        <Styled.Main>
            <TabsSwitcher tabs={tabs} />
        </Styled.Main>
    );
}

export default App;

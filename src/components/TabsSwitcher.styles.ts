import styled from '@emotion/styled';

const tabBorderWidth = '1px';

export const Main = styled.div`
    width: 800px;
    min-height: 400px;
    display: grid;
    grid-template-rows: auto 1fr;
`;

export const Tabs = styled.div`
    display: flex;
    align-items: center;
`;

export const Tab = styled.div<{ isActive: boolean }>`
    padding: 4px 16px;
    cursor: pointer;
    border: ${tabBorderWidth} solid gray;
    border-bottom: none;
    border-radius: 8px 8px 0 0;
    background-color: ${props =>
        props.isActive ? 'lightgray' : 'transparent'};

    &:not(:last-of-type) {
        margin-right: calc(-1 * ${tabBorderWidth});
    }
`;

export const Body = styled.div`
    border: 2px solid gray;
    padding: 16px;
`;

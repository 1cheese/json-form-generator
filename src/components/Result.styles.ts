import styled from '@emotion/styled';

export const Warning = styled.h2`
    color: #d8295b;
`;

export const Row = styled.div`
    display: grid;
    grid-template-columns: [label] minmax(min-content, 200px) [input] auto;
    grid-column-gap: 8px;

    &:not(:last-of-type) {
        margin-bottom: 16px;
    }
`;

export const Label = styled.div`
    grid-area: label;

    label {
        display: block;
    }
`;

export const Input = styled.div`
    grid-area: input;

    input:not([type='checkbox']):not([type='radio']):not([type='color']),
    textarea {
        width: 100%;
    }
`;

export const ButtonsWrapper = styled.div`
    margin-top: 16px;
    display: flex;
    justify-content: flex-end;
    align-items: center;

    button:not(:last-child) {
        margin-right: 8px;
    }
`;
